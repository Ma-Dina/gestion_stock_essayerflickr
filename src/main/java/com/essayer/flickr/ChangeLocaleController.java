package com.essayer.flickr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis.utils.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.essayer.flickr.utils.ApplicationUtils;

@Controller
@RequestMapping(value = "/changelocale")
public class ChangeLocaleController {

	private static final String REFERER="referer";
	
	@RequestMapping(value = "/{locale}")
	public String changeLocale(HttpServletRequest request,HttpServletResponse response,@PathVariable String locale ) {
		//if (StringUtils.isEmpty(locale)) {//si notre locale n'est p vide on le change
			ApplicationUtils.changeLocale(request, response, locale);
			
		//}
		//recup le dernier url (la variable REFEeR exist ds httpRequest ki permet de faire ca	
		String lastUrl= request.getHeader(REFERER);
		//if (StringUtils.isEmpty(lastUrl)) {//si n'est p vide faire un redirection
			return "redirect:"+lastUrl;//reactualiser la page en cour=pour birn charger le nouveau contecxt et nouv propri�t�
			
		//}
		//return "redirect:/";//si on arrive p a recup le dernier url
	}
	
}
