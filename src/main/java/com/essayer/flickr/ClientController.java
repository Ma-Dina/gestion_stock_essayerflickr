package com.essayer.flickr;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.essayer.flickr.entities.Client;
import com.essayer.flickr.services.IClientService;
import com.essayer.flickr.services.IFilckrService;



@Controller
@RequestMapping(value = "/client")
public class ClientController {

	// pr l'enegistrement
	@Autowired
	private IFilckrService filckrService ;
	
	@Autowired  //pr dire a spring ke min4 VIDEO15 le dao declar� ds beans-config
	private IClientService clientService;
	
	@RequestMapping(value = "/") //=si g /client/ afficher client.jsp ds le dosier client
	public String client(Model model) {
		//recup la liste des client
		List<Client> clients=clientService.selectAll();
		if (clients==null) {//ce test c pr eviter le bp des pointeur null
			clients=new ArrayList<Client>();
		}
		model.addAttribute("clients", clients);
		return "client/client";
	}
	
	//le roles de cett methode est de faire une redirection ajouterClient.jsp tt en passant les param neccessaire
	
	@RequestMapping(value = "/nouveau" ,method = RequestMethod.GET) // /client/nouveau j'aurai cett url qd je click sur ajouter(c contenu ds son href
	public String ajouterClient(Model model) {
		Client client=new Client();
		model.addAttribute("client", client);
		return "client/ajouterClient";
	}
	
	@RequestMapping(value = "/enregistrer")// ,method = RequestMethod.POST)//par defaut c get
	public String enregistrerClient(Model model, Client client,MultipartFile file) {
		String photoUrl=null;
		
		if (client !=null) {//normalment si g saisie les champs et on a click� sur btn enreg
// si ca marche p c 2boucle regroup� ---faire 3 psk lui il a install� un plugin
		  if (file !=null) {//faut p ke le fichier soit vide
			  InputStream stream=null;
			  try {//on +le try
				   stream=file.getInputStream();
				  photoUrl=filckrService.savePhoto(stream, client.getNom());
				  client.setPhoto(photoUrl);
			} catch (Exception e) {
				e.printStackTrace();
			}finally {//fermerer le stream
				try {//c clui ki demande le try
					stream.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			}
				
		  }
	
//on ajoute ce if pr gerer la modifierClient et l'+ car la diff entre l'+ et la modif c ke le id qd  =0 c + ;et diff de 0qd c modif
			if (client.getIdClient() !=null) {//cas de modif
				clientService.update(client);
			} else {//cas d'+
				clientService.save(client);
			}

		}
		
		return "redirect:/client/";
	}
	
	@RequestMapping(value = "/modifier/{idClient}") //1
	public String modifierClient(Model model,@PathVariable Long idClient) {//faut k ca soit le mm nom ke celui ds le requst 1 sinn on doit le preciser @PathVariable("Nom1")
		//recuperer le client de la bbdd
		if (idClient != null) {
			Client client=clientService.getById(idClient);
			if (client !=null) {
				model.addAttribute("client", client);//faut k ca soit le mm nom ke  celui de ajouterClient psk on utilise la mm page et modetAttribute ki =client;  dc j'envoie tjr le client recup pr tt ce ke je saisie ds ajouterCient amake d setNom()....
			}
		}
		
		return "client/ajouterClient";
	}
	
	@RequestMapping(value = "/supprimer/{idClient}")
	public String supprimerClient(Model model,@PathVariable Long idClient) {
		if (idClient != null) {
			Client client=clientService.getById(idClient);
			if (client !=null) {//ces test pr bien verifier si le client existe en bdd imaginons kon a 2employ� le 1er supp le client1 le 2eme n'a p actualis� la pageet veux le supp ca peut generer des exceptions 
				clientService.remove(idClient);
			}
		}
		return "redirect:/client/";
	}
}
