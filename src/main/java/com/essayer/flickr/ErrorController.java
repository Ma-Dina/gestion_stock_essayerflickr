package com.essayer.flickr;

import javax.servlet.http.HttpServletRequest;

import org.apache.axis.utils.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/403")
public class ErrorController {

	private static final String REFERER="referer";
	
	@RequestMapping(value = "/")
	public String errorPage(Model model, HttpServletRequest request) {
		//ca marche p ac la boucle if (StringUtils.isEmpty(lastUrl)) c � suuuuuuuuuuuuuuuuuup
		String lastUrl=request.getHeader(REFERER);//recup le dernier url
		//if (StringUtils.isEmpty(lastUrl)) {//dc on a l'url(auquel on a le droit dyacceder) � partire duqel on a click� pr arriver � 40
			model.addAttribute("backUrl", lastUrl);
		//}
		return"errors/403";
	}
}
