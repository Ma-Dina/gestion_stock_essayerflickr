package com.essayer.flickr;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.axis.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.apache.axis.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.essayer.flickr.entities.Article;
import com.essayer.flickr.entities.Category;
import com.essayer.flickr.services.IArticleService;
import com.essayer.flickr.services.ICategoryService;
import com.essayer.flickr.services.IFilckrService;


/**
 * Handles requests for the application home page.*/
 
@Controller
@RequestMapping(value = "/article")
public class HomeController {

		// pr l'enegistrement
		@Autowired
		private IFilckrService filckrService ;
		
		@Autowired  //pr dire a spring ke min4 VIDEO15 le dao declar� ds beans-config
		private IArticleService articleService;
		
		//ca c pr les category
		@Autowired
		private ICategoryService catService;
		
		
		@RequestMapping(value = "/") //=si g /client/ afficher client.jsp ds le dosier client
		public String article(Model model) {
		
			//recup la liste des client
			List<Article> articles=articleService.selectAll();
			if (articles==null) {//ce test c pr eviter le bp des pointeur null
				articles=new ArrayList<Article>();
			}
			model.addAttribute("articles",articles);
			
			System.out.println("Contenuuuuuuuuuuuuuuu"+articles);
			return "article/article";
		}
		
		//le roles de cett methode est de faire une redirection ajouterClient.jsp tt en passant les param neccessaire
		
		@RequestMapping(value = "/nouveau" ,method = RequestMethod.GET) // /client/nouveau j'aurai cett url qd je click sur ajouter(c contenu ds son href
		public String ajouterArticle(Model model) {
		
		  Article article=new Article(); 
		  List<Category> categories=catService.selectAll();
		  if (categories==null) { categories=new
		  ArrayList<Category>(); 
		  }
		  model.addAttribute("article", article);
		  model.addAttribute("categories", categories);
		 
			return "article/ajouterArticle";
		}
		
		@RequestMapping(value = "/enregistrer" ,method = RequestMethod.POST)//par defaut c get
		public String enregistrerArticle(Model model, Article article,MultipartFile file) {
			String photoUrl=null;
			
			if (article !=null) {//normalment si g saisie les champs et on a click� sur btn enreg
	// si ca marche p c 2boucle regroup� ---faire 3 psk lui il a install� un plugin
			  if (file !=null) {//faut p ke le fichier soit vide
				  InputStream stream=null;
				  try {//on +le try
					   stream=file.getInputStream();
					  photoUrl=filckrService.savePhoto(stream, article.getDesignation());
					  article.setPhoto(photoUrl);
				} catch (Exception e) {
					e.printStackTrace();
				}finally {//fermerer le stream
					try {//c clui ki demande le try
						stream.close();
					} catch (IOException e) {
						
						e.printStackTrace();
					}
				}
					
			  }
		
	//on ajoute ce if pr gerer la modifierClient et l'+ car la diff entre l'+ et la modif c ke le id qd  =0 c + ;et diff de 0qd c modif
				if (article.getIdArticle() !=null) {//cas de modif
					articleService.update(article);
				} else {//cas d'+
					articleService.save(article);
				}

			}
			
			return "redirect:/article/";
		}
		
		@RequestMapping(value = "/modifier/{idArticle}") //1
		public String modifierClient(Model model,@PathVariable Long idArticle) {//faut k ca soit le mm nom ke celui ds le requst 1 sinn on doit le preciser @PathVariable("Nom1")
			//recuperer le client de la bbdd
			if (idArticle != null) {
				Article article=articleService.getById(idArticle);
				List<Category> categories=catService.selectAll();
				if (categories==null) {
					categories=new ArrayList<Category>(); 
				}
				model.addAttribute("categories", categories);
				if (article !=null) {
					model.addAttribute("article", article);//faut k ca soit le mm nom ke  celui de ajouterClient psk on utilise la mm page et modetAttribute ki =client;  dc j'envoie tjr le client recup pr tt ce ke je saisie ds ajouterCient amake d setNom()....
				}
			}
			
			return "article/ajouterArticle";
		}
		
		@RequestMapping(value = "/supprimer/{idArticle}")
		public String supprimerArticle(@PathVariable Long idArticle) {
			if (idArticle != null) {
				Article article=articleService.getById(idArticle);
				if (article !=null) {//ces test pr bien verifier si le client existe en bdd imaginons kon a 2employ� le 1er supp le client1 le 2eme n'a p actualis� la pageet veux le supp ca peut generer des exceptions 
					articleService.remove(idArticle);
				}
			}
			return "redirect:/article/";
		}
	}

	
	/*private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	private static final String REFERER="referer";
	/**
	 * Simply selects the home view to render by returning its name.
	 
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		
		
		return "article/article";//"blank";//"home";
	}
	
	@RequestMapping(value = "/error")
	public String erreur( Model model, HttpServletRequest request) {
		
		String lastUrl=request.getHeader(REFERER);//recup le dernier url
		//if (StringUtils.isEmpty(lastUrl)) {//dc on a l'url(auquel on a le droit dyacceder) � partire duqel on a click� pr arriver � 40
			model.addAttribute("backUrl", lastUrl);
		//}
		
		return "403";//"blank";//"home";
	}*/

