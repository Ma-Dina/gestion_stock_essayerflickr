package com.essayer.flickr.dao;

import com.essayer.flickr.entities.Article;

public interface IArticleDao extends IGenericDao<Article>{

}
