package com.essayer.flickr.dao;

import com.essayer.flickr.entities.Category;

public interface ICategoryDao extends IGenericDao<Category> {

}
