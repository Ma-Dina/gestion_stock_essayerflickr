package com.essayer.flickr.dao;

import com.essayer.flickr.entities.Client;

public interface IClientDao extends IGenericDao<Client> {

}
