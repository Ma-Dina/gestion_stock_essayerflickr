package com.essayer.flickr.dao;

import com.essayer.flickr.entities.CommandeClient;

public interface ICommandeClientDao extends IGenericDao<CommandeClient> {

}
