package com.essayer.flickr.dao;

import java.util.List;

//<E>=interface generiUE ki regroupe tt les methodes communes entre tt les dao com l'+ la -
public interface IGenericDao<E> {
	
	public E save(E entity);
	
	public E update(E entity);
	
	public List<E> selectAll();
	
	public List<E> selectAll(String sortField, String sort);//l'equivalent de select* from table mais en + 2param ki permettent de faire le tri
	
	public E getById(Long id);
	
	public void remove(Long id);
	
	public E findOne(String paramName, Object paramValue);
	
	public E findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, Object paramValue) ;
	
}
