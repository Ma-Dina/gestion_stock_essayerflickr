package com.essayer.flickr.dao;

import java.util.List;

import com.essayer.flickr.entities.LigneCommandeClient;

public interface ILigneCommandeClientDao extends IGenericDao<LigneCommandeClient>{

	public List<LigneCommandeClient> getByIdCommande(Long idCommandeClient);
}
