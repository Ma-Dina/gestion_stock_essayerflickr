package com.essayer.flickr.dao;

import com.essayer.flickr.entities.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurDao extends IGenericDao<LigneCommandeFournisseur> {

}
