package com.essayer.flickr.dao;

import com.essayer.flickr.entities.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur>{

}
