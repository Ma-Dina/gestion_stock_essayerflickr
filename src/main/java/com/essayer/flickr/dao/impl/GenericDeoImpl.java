package com.essayer.flickr.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.essayer.flickr.dao.IGenericDao;



@SuppressWarnings("unchecked")//celui la on l'obtien getActualTypeArguments add
public class GenericDeoImpl<E> implements IGenericDao<E> {

	@PersistenceContext
	EntityManager en;
	
	private Class<E> type;
	
	
	public Class<E> getType() {
		return type;
	}

	public GenericDeoImpl() {
		Type t=getClass().getGenericSuperclass();
		ParameterizedType pt=(ParameterizedType) t;
		type=(Class<E>) pt.getActualTypeArguments()[0];
	}
	
	@Override
	public E save(E entity) {
		en.persist(entity);
		return entity;
	}

	@Override
	public E update(E entity) {
		return en.merge(entity);
		
	}

	@Override
	public List<E> selectAll() {
		System.out.println("voiliiiiivoilouuuuu"+type.getSimpleName());
		Query query=en.createQuery("select t from "+type.getSimpleName()+" t");//c du jpql p sql standard
		List<E> liste=query.getResultList();
		return liste;
	}

	@Override
	public List<E> selectAll(String sortField, String sort) {
		Query query=en.createQuery("select t from "+type.getSimpleName()+" t order by "+sortField+" "+sort);
		return query.getResultList();
	}

	@Override
	public E getById(Long id) {
		return en.find(type,id);
	
	}

	@Override
	public void remove(Long id) {
		E tab=en.getReference(type, id);
		en.remove(tab);
		
	}

	@Override
	public E findOne(String paramName, Object paramValue) {
		
		Query query=en.createQuery("select t from "+type.getSimpleName()+" t where "+paramName+" = :x");//=:x pr passer des param en jpql
		query.setParameter(paramName,paramValue);
		return query.getResultList().size() > 0 ? (E) query.getResultList().get(0):null;
		//if (query.getResultList().size() > 0 ){ return  (E) query.getResultList().get(0)} return null;
	}

	@Override
	public E findOne(String[] paramNames, Object[] paramValues) {
		
		if (paramNames.length!=paramValues.length) {
			return null;
		}
		
		String queryString="select e from "+type.getSimpleName()+" e where";
		int len=paramNames.length;//pr avoir le nbr de param
		for (int i = 0; i <len; i++) {
			queryString += "e." +paramNames[i] +"= :x"+i;
			if ((i+1)<len) {
				queryString += " and ";
			}
		}
		Query query=en.createQuery(queryString);
		for (int i = 0; i < paramValues.length; i++) {
			query.setParameter("x" + i, paramValues[i]);
		}
		return query.getResultList().size() > 0 ? (E) query.getResultList().get(0):null;

	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		Query query=en.createQuery("select t from "+type.getSimpleName()+" t where "+paramName+" = :x");//=:x pr passer des param en jpql
		query.setParameter(paramName,paramValue);
		return query.getResultList().size() > 0 ?((Long) query.getSingleResult()).intValue():0;
		
	
	}

}
