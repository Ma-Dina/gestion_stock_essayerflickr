package com.essayer.flickr.entities;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;



@Entity
public class CommandeClient implements Serializable {

	@Id
	@GeneratedValue
	private Long idCommandeClient;

	private String code;

	@Temporal(TemporalType.TIMESTAMP) // prendre la date et l'heur
	private Date dateCommande;

	@ManyToOne
	@JoinColumn(name = "idClient")
	private Client client;

	@OneToMany(mappedBy = "commandeClient")
	private List<LigneCommandeClient> ligneCommandeClients;

	/*
	 * @Transient private String ligneCommandeJson;
	 */

	@Transient // permet de dire a hebernate lor de la creation ou modif de la bdd si il trouve
				// un att annot� @transient il va p le creer ds la BDD
	private BigDecimal totalCommande;

	public Long getId() {
		return idCommandeClient;
	}

	public void setId(Long id) {
		this.idCommandeClient = id;
	}

	public Long getIdCommandeClient() {
		return idCommandeClient;
	}

	public void setIdCommandeClient(Long idCommandeClient) {
		this.idCommandeClient = idCommandeClient;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateCommande() {
		return dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	// @JsonIgnore
	public List<LigneCommandeClient> getLigneCommandeClients() {
		return ligneCommandeClients;
	}

	public void setLigneCommandeClients(List<LigneCommandeClient> ligneCommandeClients) {
		this.ligneCommandeClients = ligneCommandeClients;
	}

	public BigDecimal getTotalCommande() {
		totalCommande = BigDecimal.ZERO;// initialiser

		if (!ligneCommandeClients.isEmpty()) {
			for (LigneCommandeClient ligneCommandeClient : ligneCommandeClients) {
				// e if on la ajout� VID36 qd on a supp le @jsonIgnore g att article
				// LigneCmdCLIENR
				if (ligneCommandeClient.getQuantite() != null && ligneCommandeClient.getPrixUnitaire() != null) {

					BigDecimal totalLigne = ligneCommandeClient.getQuantite()
							.multiply(ligneCommandeClient.getPrixUnitaire());
					totalCommande = totalCommande.add(totalLigne);

				}
			}
		}

		return totalCommande;
	}

	/*
	 * @Transient //pr transformer l'objeet ligneCommandeClients en ob json public
	 * String getLigneCommandeJson() {
	 * 
	 * if (ligneCommandeClients.isEmpty()) {
	 * 
	 * try { la dependence jaxen ns permet de manipuler et convertir des ob java en
	 * json (et le contraire) via la classe ObjectMapper ObjectMapper mapper=new
	 * ObjectMapper(); return mapper.writeValueAsString(ligneCommandeClients); }
	 * catch (JsonGenerationException e) { e.printStackTrace(); } catch
	 * (JsonMappingException e) { e.printStackTrace(); } catch (IOException e) {
	 * e.printStackTrace(); } }
	 * 
	 * return ""; }
	 */

}
