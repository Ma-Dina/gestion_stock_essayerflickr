package com.essayer.flickr.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class LigneCommandeClient implements Serializable{

	@Id
	@GeneratedValue
	private Long idLigneCdeClt;
	
	@ManyToOne   
	@JoinColumn(name = "idArticle")
	private Article article;
	
	@ManyToOne   
	@JoinColumn(name = "idCommandeClient")
	private CommandeClient commandeClient;
	
	private BigDecimal quantite;
	private BigDecimal prixUnitaire;//mm si ya le prixUnitaire ds article com attr mais si on achete un article mnt ;un mois apres le prix augment dc pr garder la tracabilité (le prix qd on la acheté)

	public Long getId() {
		return idLigneCdeClt;
	}

	public LigneCommandeClient() {
		super();
	}
	
	public Long getIdLigneCdeClt() {
		return idLigneCdeClt;
	}



	public void setIdLigneCdeClt(Long idLigneCdeClt) {
		this.idLigneCdeClt = idLigneCdeClt;
	}


	 
	public Article getArticle() {
		return article;
	}



	public void setArticle(Article article) {
		this.article = article;
	}


	/*
	 * @JsonIgnore //pr eviter le lazy load exception
	 */	public CommandeClient getCommandeClient() {
		return commandeClient;
	}



	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	}



	public void setId(Long id) {
		this.idLigneCdeClt = id;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public BigDecimal getPrixUnitaire() {
		return prixUnitaire;
	}

	public void setPrixUnitaire(BigDecimal prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}
	
	
}
