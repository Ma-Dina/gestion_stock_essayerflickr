package com.essayer.flickr.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LingneVente implements Serializable{

	@Id
	@GeneratedValue
	private Long idLingneVente;
	
	@ManyToOne   
	@JoinColumn(name = "idArticle")
	private Article article;
	
	@ManyToOne   
	@JoinColumn(name = "vente")
	private Vente vente;

	public Long getId() {
		return idLingneVente;
	}

	public void setId(Long id) {
		this.idLingneVente = id;
	}

	public Long getIdLingneVente() {
		return idLingneVente;
	}

	public void setIdLingneVente(Long idLingneVente) {
		this.idLingneVente = idLingneVente;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
	
}
