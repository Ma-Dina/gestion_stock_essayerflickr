package com.essayer.flickr.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MvtStck implements Serializable{

	public static final int ENTREE=1;
	
	public static final int SORTIE=2;
	
	@Id
	@GeneratedValue
	private Long idMvtStck;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvt;
	
	private BigDecimal quantities;
	
	private int typeMvt;
	
	@ManyToOne   
	@JoinColumn(name = "idArticle")
	private Article article;
	
	public Long getId() {
		return idMvtStck;
	}

	public void setId(Long id) {
		this.idMvtStck = id;
	}

	public Long getIdMvtStck() {
		return idMvtStck;
	}

	public void setIdMvtStck(Long idMvtStck) {
		this.idMvtStck = idMvtStck;
	}

	public Date getDateMvt() {
		return dateMvt;
	}

	public void setDateMvt(Date dateMvt) {
		this.dateMvt = dateMvt;
	}

	public BigDecimal getQuantities() {
		return quantities;
	}

	public void setQuantities(BigDecimal quantities) {
		this.quantities = quantities;
	}

	public int getTypeMvt() {
		return typeMvt;
	}

	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
	
}
