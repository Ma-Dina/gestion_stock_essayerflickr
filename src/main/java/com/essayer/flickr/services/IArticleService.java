package com.essayer.flickr.services;

import java.util.List;

import  com.essayer.flickr.entities.Article;

public interface IArticleService {

public Article save(Article entity);
	
	public Article update(Article entity);
	
	public List<Article> selectAll();
	
	public List<Article> selectAll(String sortField, String sort);//l'equivalent de select* from table mais en + 2param ki permettent de faire le tri
	
	public Article getById(Long id);
	
	public void remove(Long id);
	
	public Article findOne(String paramName, Object paramValue);
	
	public Article findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, Object paramValue) ;

	
}
