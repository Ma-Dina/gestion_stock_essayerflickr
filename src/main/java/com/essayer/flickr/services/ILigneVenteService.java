package com.essayer.flickr.services;

import java.util.List;

import  com.essayer.flickr.entities.Article;
import com.essayer.flickr.entities.LingneVente;

public interface ILigneVenteService {

public LingneVente save(LingneVente entity);
	
	public LingneVente update(LingneVente entity);
	
	public List<LingneVente> selectAll();
	
	public List<LingneVente> selectAll(String sortField, String sort);
	
	public LingneVente getById(Long id);
	
	public void remove(Long id);
	
	public LingneVente findOne(String paramName, Object paramValue);
	
	public LingneVente findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, Object paramValue) ;


	
}
