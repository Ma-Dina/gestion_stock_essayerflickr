package com.essayer.flickr.services;

import java.util.List;

import  com.essayer.flickr.entities.Article;
import com.essayer.flickr.entities.MvtStck;

public interface IMvtStckService {

public MvtStck save(MvtStck entity);
	
	public MvtStck update(MvtStck entity);
	
	public List<MvtStck> selectAll();
	
	public List<MvtStck> selectAll(String sortField, String sort);
	
	public MvtStck getById(Long id);
	
	public void remove(Long id);
	
	public MvtStck findOne(String paramName, Object paramValue);
	
	public MvtStck findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, Object paramValue) ;


	
}
