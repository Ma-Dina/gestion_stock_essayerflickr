package com.essayer.flickr.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.essayer.flickr.dao.IArticleDao;
import com.essayer.flickr.entities.Article;
import com.essayer.flickr.services.IArticleService;


@Transactional  //je v faire une transaction av la bdd
public class ArticleServiceImpl implements IArticleService{

	/*
	 * le bean declar� ds applicationContext.xml 
	 <bean id="articleDao" class="com.stock.mvc.dao.impl.ArticleDaoImpl"/>
<bean id="articleService" class="com.stock.mvc.services.impl.ArticleServiceImpl">
	<property name="dao" ref="articleDao"></property></bean>
	*
	*SON EQUIVALENT EN JAVA C:
private IArticleDao dao=new ArticleDaoImpl();
***********-MAIS
* avec cet methode on aura un couplage fort= si je venais a modifieier un article je 
* le ferais partt ds service...
au lieu de cela on le modifie juste ds applicationContext.xml da ref

	 * */
	
	private IArticleDao dao;//psk il contioent tte les methodes
	

	public void setDao(IArticleDao dao) {
		this.dao = dao;
	}

	@Override
	public Article save(Article entity) {
		
		return dao.save(entity);
	}

	@Override
	public Article update(Article entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<Article> selectAll() {
		
		return dao.selectAll();
	}

	@Override
	public List<Article> selectAll(String sortField, String sort) {
		
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Article getById(Long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public Article findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Article findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
