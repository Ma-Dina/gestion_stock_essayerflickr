package com.essayer.flickr.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.essayer.flickr.dao.ICommandeClientDao;
import com.essayer.flickr.entities.CommandeClient;
import com.essayer.flickr.services.ICommandeClientService;


@Transactional
public class CommandeClientServiceImpl implements ICommandeClientService{

private ICommandeClientDao dao;
	

	public void setDao(ICommandeClientDao dao) {
		this.dao =dao;
	}
	
	@Override
	public CommandeClient save(CommandeClient entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public CommandeClient update(CommandeClient entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<CommandeClient> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<CommandeClient> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public CommandeClient getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public CommandeClient findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeClient findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

}
