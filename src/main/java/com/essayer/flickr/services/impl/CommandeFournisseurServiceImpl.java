package com.essayer.flickr.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.essayer.flickr.dao.ICommandeFournisseurDao;
import com.essayer.flickr.entities.CommandeFournisseur;
import com.essayer.flickr.services.ICommandeFournisseurService;


@Transactional
public class CommandeFournisseurServiceImpl implements ICommandeFournisseurService{

private ICommandeFournisseurDao dao;
	

	public void setDao(ICommandeFournisseurDao dao) {
		this.dao =dao;
	}
	
	@Override
	public CommandeFournisseur save(CommandeFournisseur entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public CommandeFournisseur update(CommandeFournisseur entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<CommandeFournisseur> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<CommandeFournisseur> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public CommandeFournisseur getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public CommandeFournisseur findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeFournisseur findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

}
