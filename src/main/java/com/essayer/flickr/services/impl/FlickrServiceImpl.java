package com.essayer.flickr.services.impl;

import java.io.InputStream;

import com.essayer.flickr.dao.IFlickerDao;
import com.essayer.flickr.services.IFilckrService;


public class FlickrServiceImpl implements IFilckrService{

	private IFlickerDao dao;
	
	public void setDao(IFlickerDao dao) {
		this.dao = dao;
	}
	
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		// TODO Auto-generated method stub
		return dao.savePhoto(photo, title);
	}

}
