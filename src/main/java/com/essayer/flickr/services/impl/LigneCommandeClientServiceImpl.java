package com.essayer.flickr.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.essayer.flickr.dao.ILigneCommandeClientDao;
import com.essayer.flickr.entities.LigneCommandeClient;
import com.essayer.flickr.services.ILigneCommandeClientService;


@Transactional
public class LigneCommandeClientServiceImpl implements ILigneCommandeClientService{

private ILigneCommandeClientDao dao;
	

	public void setDao(ILigneCommandeClientDao dao) {
		this.dao =dao;
	}
	
	@Override
	public LigneCommandeClient save(LigneCommandeClient entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public LigneCommandeClient update(LigneCommandeClient entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<LigneCommandeClient> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<LigneCommandeClient> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneCommandeClient getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public LigneCommandeClient findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCommandeClient findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

	@Override
	public List<LigneCommandeClient> getByIdCommande(Long idCommandeClient) {
		
		return dao.getByIdCommande(idCommandeClient);
	}

}
