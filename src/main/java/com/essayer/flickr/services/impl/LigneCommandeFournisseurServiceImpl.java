package com.essayer.flickr.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.essayer.flickr.dao.ILigneCommandeFournisseurDao;
import com.essayer.flickr.entities.LigneCommandeFournisseur;
import com.essayer.flickr.services.ILigneCommandeFournisseurService;


@Transactional
public class LigneCommandeFournisseurServiceImpl implements ILigneCommandeFournisseurService{

private ILigneCommandeFournisseurDao dao;
	

	public void setDao(ILigneCommandeFournisseurDao dao) {
		this.dao =dao;
	}


	@Override
	public LigneCommandeFournisseur save(LigneCommandeFournisseur entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}


	@Override
	public LigneCommandeFournisseur update(LigneCommandeFournisseur entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}


	@Override
	public List<LigneCommandeFournisseur> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}


	@Override
	public List<LigneCommandeFournisseur> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}


	@Override
	public LigneCommandeFournisseur getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}


	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}


	@Override
	public LigneCommandeFournisseur findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}


	@Override
	public LigneCommandeFournisseur findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}


	@Override
	public int findCountBy(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}
	
	}
