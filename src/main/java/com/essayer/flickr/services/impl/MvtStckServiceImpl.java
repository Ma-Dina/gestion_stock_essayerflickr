package com.essayer.flickr.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.essayer.flickr.dao.IMvtStckDao;
import com.essayer.flickr.entities.MvtStck;
import com.essayer.flickr.services.IMvtStckService;


@Transactional
public class MvtStckServiceImpl implements IMvtStckService{

private IMvtStckDao dao;
	

	public void setDao(IMvtStckDao dao) {
		this.dao =dao;
	}
	
	@Override
	public MvtStck save(MvtStck entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public MvtStck update(MvtStck entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<MvtStck> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<MvtStck> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public MvtStck getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public MvtStck findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public MvtStck findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

}
