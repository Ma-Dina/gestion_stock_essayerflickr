package com.essayer.flickr.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.essayer.flickr.dao.IVenteDao;
import com.essayer.flickr.entities.Vente;
import com.essayer.flickr.services.IVenteService;


@Transactional
public class VenteServiceImpl implements IVenteService{

private IVenteDao dao;
	

	public void setDao(IVenteDao dao) {
		this.dao =dao;
	}
	
	@Override
	public Vente save(Vente entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public Vente update(Vente entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<Vente> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<Vente> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Vente getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public Vente findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Vente findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

}
