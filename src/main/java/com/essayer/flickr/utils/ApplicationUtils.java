package com.essayer.flickr.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

//mettre des methde statc
public class ApplicationUtils {

	//constructeur priv� pr empecher l'instanciation de la classe
	private ApplicationUtils() {}
	
	//cette methode on l'appel a chaque fois qd veux changer le locale+crrer en controller ki permet de faire ca
	public static void changeLocale(HttpServletRequest request,HttpServletResponse response,String locale) {
		LocaleResolver localeResolver=RequestContextUtils.getLocaleResolver(request);//recuperer le loclae deja existant dans la request
		localeResolver.setLocale(request, response, StringUtils.parseLocaleString(locale));
	}
}
