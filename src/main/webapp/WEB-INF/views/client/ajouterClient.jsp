     
<!DOCTYPE html>
<html lang="fr">




<!-- izmer lhaalll en hauttttttttttttttttttttttttttttttttttttt -->
<%@include file="/WEB-INF/views/includes/includes.jsp" %>



<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Gestion de stock</title>

  <!-- Custom fonts for this template-->
  <link href="<%=request.getContextPath() %>/resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<%=request.getContextPath() %>/resources/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

<!--  Sidebar -->
 <%@include file="/WEB-INF/views/code/leftMenuSB.jsp" %>
<!-- End of Sidebar -->


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

<!-- End of Topbar -->
 <%@include file="/WEB-INF/views/code/topMenu.jsp" %>
<!-- End of Topbar -->
       
       
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"><fmt:message code="client.nouveau"/></h1>





<!-- ______________________________CE MORCEAU + client.jsp______________________________________________________________ -->
         <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><fmt:message code="client.nouveau"/></h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
 
 <!-- ___________________________LE FORMULAIRE D'AJOUT ICI_____________________________________________________________________________________ -->
<c:url value="/client/enregistrer" var="urlEnregistrer" /> <!-- PSK SI ON DONNE DIRECTEMENT L'URL DS ACTION IL VA PRENDRE JUSTE CET URL /client/enregistrer SANS LE /mvc AVNT DC AC LA BALISE c:url ON ASSURE L'URL COMPLETT!!! --> 
 <f:form modelAttribute="client" action="${urlEnregistrer }"  enctype="multipart/form-data" role="form"><!--  UTILISER LES SPRING FORM pr utiliser modelAttribute=getAttribute; enctype pr dire ke je v faire un transfert de fichier tupe file( envoyer un fichier pr pouvoire l'enregistrer -->
  
  <f:hidden path="idClient"/><!--  GARDER LE  ID ET PHOTO CAS DE MODIF -->
  <f:hidden path="photo"/>
  
  <div class="form-group">
    <label><fmt:message code="common.nom"/></label>
     <f:input path="nom" class="form-control" placeholder="Nom"/> <!-- LE CHAMPS KI REPRESENTE LE NOM DE L'OPBJ modelAttribute="client"(DECLARE DS LE controllerClient) C CET INPUTE ET SON PATH C nom celui declar� ds entit� Client [dc ce ke je saisie ici amaken setNom] waqil si c vide il affich le placeHolder sinn sa val -->
  </div>
  
  <div class="form-group">
    <label><fmt:message code="common.prenom"/></label>
     <f:input path="prenom" class="form-control" placeholder="Prenom"/>
  </div>
  
  <div class="form-group">
    <label><fmt:message code="common.adresse"/></label>
     <f:input path="adresse" class="form-control" placeholder="Adresse"/>
  </div>
  
  <div class="form-group">
    <label><fmt:message code="common.mail"/></label>
     <f:input path="mail" class="form-control" placeholder="Mail"/>
  </div>
  
   <div class="form-group">
    <label><fmt:message code="common.photo"/></label>
     <input type="file" name="file"/>
  </div>
  
  <div class="panel-footer">
    <button type="submit" class="btn btn-primary"><i class="fa fa-save">&nbsp; <fmt:message code="common.enregistrer"/></i></button>
    <a href="<c:url value="/client/" />" class="btn btn-danger"><i class="fa fa-arrow-left">&nbsp;<fmt:message code="common.annuler"/></i></a>
  </div>
 </f:form>
<!-- _____________________________________FIN FORMULAIRE D'AJOUT___________________________________________________________________________ -->
 
 
              </div>
            </div>
          </div>
<!-- _______________________________________________________________________________________________ -->


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<%=request.getContextPath() %>/resources/vendor/jquery/jquery.min.js"></script>
  <script src="<%=request.getContextPath() %>/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<%=request.getContextPath() %>/resources/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<%=request.getContextPath() %>/resources/js/sb-admin-2.min.js"></script>

</body>

</html>
