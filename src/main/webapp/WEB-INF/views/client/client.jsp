     
<!DOCTYPE html>
<html lang="fr">




<!-- izmer lhaalll en hauttttttttttttttttttttttttttttttttttttt -->
<%@include file="/WEB-INF/views/includes/includes.jsp" %>



<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Gestion de stock</title>

  <!-- Custom fonts for this template-->
  <link href="<%=request.getContextPath() %>/resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<%=request.getContextPath() %>/resources/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="<%=request.getContextPath() %>/resources/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

<!--  Sidebar -->
 <%@include file="/WEB-INF/views/code/leftMenuSB.jsp" %>
<!-- End of Sidebar -->


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

<!-- End of Topbar -->
 <%@include file="/WEB-INF/views/code/topMenu.jsp" %>
<!-- End of Topbar -->
       
       
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"><fmt:message code="common.client"/></h1>


<!-- __--------______-------__-ceci g lai + manuelel de lavideo -->
<div class="row">
<div class="col-lg-12">
 	<ol class="breadcrumb">
 		<li><a href="<c:url value="/client/nouveau" />">&nbsp;&nbsp;<i class="fa fa-plus">&nbsp;<fmt:message code="common.ajouter"/></i> </a> </li> 
  		<li><a href="">&nbsp;&nbsp;<i class="fa fa-upload">&nbsp;<fmt:message code="common.exporter"/></i></a> </li>
 		<li><a href="">&nbsp;&nbsp;<i class="fa fa-download">&nbsp;<fmt:message code="common.importer"/></i></a> </li>
 	</ol>
</div>

</div>

<!-- ____________________________________________________________________________________________ -->
         <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><fmt:message code="client.liste"/></h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    <th><fmt:message code="common.photo"/></th>
                     <th><fmt:message code="common.nom"/></th>
                      <th><fmt:message code="common.prenom"/></th>
                      <th><fmt:message code="common.adresse"/></th>
                      <th><fmt:message code="common.mail"/></th>
                      <th><fmt:message code="common.actions"/></th>
                      
                    </tr>
                  </thead>
               
                  <tbody>
                  <c:forEach items="${clients }" var="client">
                  
                  	  <tr>
                      <td class="centre"> <img alt="" src="${client.getPhoto() }" width="50px" height="50px"> </td>
                      <td>${client.getNom() }</td>
                      <td>${client.getPrenom() }</td>
                      <td>${client.getAdresse() }</td>
                      <td>${client.getMail() }</td>
                      <td>
                      <c:url value="/client/modifier/${client.getIdClient() }" var="urlModif" />
						 <a href="${urlModif }"><i class="fas fa-check"></i></a> <!-- ou class="fa fa-edit" -->
						 &nbsp;|&nbsp; 
						<a href="javascript:void(0)" data-toggle="modal" data-target="#modalClient${client.getIdClient() }"><i class="fas fa-trash"></i></a> <!-- ou class="fa fa-trash-o" 
 						 
 						 ac data-toggle="modal" data-target="#myModal" on lie le pop up au btn supp
 						 modalClient${client.getIdClient() } pr ke chaque client aie son modal-->
<!-- _______________________________________POP UP_____________________________________________________________________________________________________________
 -->
<!-- Trigger the modal with a button -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

<!-- Modal -->
<div id="modalClient${client.getIdClient() }" class="modal fade" role="dialog" aria-hidden="true" aria-labelledby="myModalLabel">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><fmt:message code="common.confirm.suppression"/></h4>
      </div>
      <div class="modal-body">
        <p><fmt:message code="client.confirm.suppression.msg"/></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message code="common.annuler"/></button>
        
        
<!-- _______________________________________KLK AJOUTTTTT pr POUVOIR SUUUUPPPPP_____________________________________________________________________________________________________________ --> 						 

         <c:url value="/client/supprimer/${client.getIdClient() }" var="urlSuppression" />
		<a href="${urlSuppression }" class="btn btn-danger"><i class="fas fa-trash"></i> &nbsp;<fmt:message code="common.confirmer"/></a>
      </div>
    </div>

  </div>
</div>
<!-- ____________________________________________________________________________________________________________________________________________________ --> 						 
 						 
 						 
					  </td>
                    </tr>
                  
                  </c:forEach>
  
                  </tbody>
                </table>
              </div>
            </div>
          </div>
<!-- _______________________________________________________________________________________________ -->
        </div>
        <!-- /.container-fluid -->
        
        
<!-- -------------------_______________________________________________________________________ -->

<!-- ---------___________________________________________________________------------> 
     </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<%=request.getContextPath() %>/resources/vendor/jquery/jquery.min.js"></script>
  <script src="<%=request.getContextPath() %>/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<%=request.getContextPath() %>/resources/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<%=request.getContextPath() %>/resources/js/sb-admin-2.min.js"></script>


  <!-- Page level plugins -->
  <script src="<%=request.getContextPath() %>/resources/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<%=request.getContextPath() %>/resources/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<%=request.getContextPath() %>/resources/js/demo/datatables-demo.js"></script>
  
  
    <!-- Bootstrap core JavaScript-->
  <script src="<%=request.getContextPath() %>vendor/jquery/jquery.min.js"></script>
  <script src="<%=request.getContextPath() %>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<%=request.getContextPath() %>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<%=request.getContextPath() %>js/sb-admin-2.min.js"></script>
  
</body>

</html>
