
    
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
<!-- ici je veux blank page je depace mvc 
c�d localhost:8080/mvc/home/ on prend /home/ ki veut dire av ce ki ya ds value="" je complet localhost:8080/mvc
p la peine de la jsp psk elle est deja definie ds le cpntroller -->
     
<c:url value="/home/" var="home" /> 
      <li class="nav-item">
        <a class="nav-link" href="${home }">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span> <fmt:message key="common.dashbord"/> </span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
    <c:url value="/article/" var="article" /> 
      <li class="nav-item">
        <a class="nav-link collapsed" href="${article }" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span><fmt:message key="common.article"/></span>
        </a>
<!--         <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Components:</h6>
            <a class="collapse-item" href="buttons.html">Buttons</a>
            <a class="collapse-item" href="cards.html">Cards</a>
          </div>	
        </div> -->
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span><fmt:message key="common.client"/></span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Utilities:</h6>
<c:url value="/client/" var="client" />
            <a class="collapse-item" href="${client }"><fmt:message key="common.client"/></a>
 <c:url value="/commandeclient/" var="cdeClient" />
            <a class="collapse-item" href="${cdeClient }"><fmt:message key="common.client.commande"/></a>
            <a class="collapse-item" href="utilities-animation.html">Animations</a>
            <a class="collapse-item" href="utilities-other.html">Other</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Addons
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item active">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span><fmt:message key="common.fournisseur"/></span>
        </a>
        <div id="collapsePages" class="collapse show" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
<c:url value="/fournisseur/" var="fournisseur" />
            <a class="collapse-item" href="${fournisseur }"><fmt:message key="common.fournisseur"/></a>
<c:url value="/commandefournisseur/" var="cdefournisseur" />
            <a class="collapse-item" href="${cdefournisseur }"><fmt:message key="common.fournisseur.commande"/></a>
            <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Other Pages:</h6>
            
            <a class="collapse-item" href="404.html">404 Page</a>
            <a class="collapse-item active" href="blank.html">Blank Page</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Charts -->
<c:url value="/stock/" var="stock" />
      <li class="nav-item">
        <a class="nav-link" href="${stock }">
          <i class="fas fa-fw fa-chart-area"></i>
          <span><fmt:message key="common.stock"/></span></a>
      </li>

      <!-- Nav Item - Tables -->
<c:url value="/vente/" var="vente" />
      <li class="nav-item">
        <a class="nav-link" href="${vente }">
          <i class="fas fa-fw fa-table"></i>
          <span><fmt:message key="common.vente"/></span></a>
      </li>
      
      
      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item active">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span><fmt:message key="common.parametrage"/></span>
        </a>
        <div id="collapsePages" class="collapse show" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
<c:url value="/utilisateur/" var="user" /> 
            <a class="collapse-item" href="${user }"><fmt:message key="common.parametrage.utilisateur"/></a>
<c:url value="/category/" var="category" /> 
            <a class="collapse-item" href="${category }"><fmt:message key="common.parametrage.category"/></a>
           
          </div>
        </div>
      </li>
      
      

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->