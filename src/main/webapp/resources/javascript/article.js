/*qd le chargement de la page se fait je veux appliquer ce js(ou evenemrnt)*/
$(document).ready(function(){ 
	/*# pr dire q'on utilise un identifiant (ceux declarées ds ajouterArticle.jsp*/
	$("#tauxTva").on("keyup",function() { /* qd on saisie sur le champ tva on declanche un evnt*/
		tvaKeyUpFunction();
	});
});

tvaKeyUpFunction = function() {
	/*RECUPERER les 2 val des champs HT et Tva pr calculer TTC */
	var prixUnitHT= $("#prixUnitaireHT").val();
	var tauxTva= $("#tauxTva").val();
	var prixUnitTTC= parseFloat( parseFloat(prixUnitHT) * parseFloat(tauxTva) /100 + parseFloat(prixUnitHT));/*faire la conversion on double pr afficher la , ds les grands numero*/
	$("#prixUnitaireTTC").val(prixUnitTTC);
}